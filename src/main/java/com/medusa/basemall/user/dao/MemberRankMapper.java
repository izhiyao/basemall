package com.medusa.basemall.user.dao;

import com.medusa.basemall.core.Mapper;
import com.medusa.basemall.user.entity.MemberRank;

/**
 * @author whh
 */
public interface MemberRankMapper extends Mapper<MemberRank> {


}