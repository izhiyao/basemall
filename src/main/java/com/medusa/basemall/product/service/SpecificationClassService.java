package com.medusa.basemall.product.service;

import com.medusa.basemall.core.Service;
import com.medusa.basemall.product.entity.SpecificationClass;

/**
 * Created by psy on 2018/05/24.
 */
public interface SpecificationClassService extends Service<SpecificationClass> {

}
