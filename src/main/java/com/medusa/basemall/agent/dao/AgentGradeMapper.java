package com.medusa.basemall.agent.dao;

import com.medusa.basemall.agent.entity.AgentGrade;
import com.medusa.basemall.core.Mapper;

public interface AgentGradeMapper extends Mapper<AgentGrade> {
}