package com.medusa.basemall.constant;

public interface VersionNumber {

	/**
	 *基础版本
	 */
	int BASICVERSION = 1;
	/**
	 *标准版本
	 */
	int STANDARDVERSION = 2;
	/**
	 *营销版本
	 */
	int MARKETINGVERSION = 3;

}
