package com.medusa.basemall.constant;

/**
 * 活动状态
 *
 * @author Created by wx on 2018/09/01.
 */
public interface ActivityState {

	/**
	 * 结束状态
	 */
	Integer OVER = 0;

	/**
	 * 未开始状态
	 */
	Integer READY = 1;

	/**
	 * 进行中状态
	 */
	Integer STARE = 2;
}
