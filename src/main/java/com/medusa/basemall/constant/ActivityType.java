package com.medusa.basemall.constant;


/**
 * 活动类型
 *
 * @author whh
 */
public interface ActivityType {

    /**
     * 优惠券活动
     */
    Integer COUPON = 1001;

    /**
     * 特价活动
     */
    Integer SPECIAL = 2001;

    /**
     * 拼团活动
     */
    Integer GROUP = 3001;

    /**
     * 秒杀活动
     */
    Integer SECONDKILL = 4001;

    /**
     * 满减
     */
    Integer  FULLSUBTRACTION = 5001;

}
